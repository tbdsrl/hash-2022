class Collaborator:
    def __init__(self, name):
        self.name = name
        self.skills = {}
        self.release_day = None
        self.project = None

    def addSkill(self, name, level):
        self.skills[name] = level

    @property
    def skill_level(self):
        return sum(self.skills.values())

    def assign(self, current_day, project, skill):
        self.release_day = current_day + project.days
        self.project = project
        self.skill = skill

    def is_free(self, current_day):
        if self.release_day:
            return self.release_day >= current_day
        return True

    def level_up_skill(self, skill_name):
        self.skills[skill_name] += 1

class Project:
    def __init__(self, name, days, score, best_before, nummber_of_roles):
        self.name = name
        self.days = int(days)
        self.score = int(score)
        self.best_before = int(best_before)
        self.number_of_roles = int(nummber_of_roles)
        self.required_skills = []
        self.completition_day = None

    def add_required_skills(self, name, level):
        self.required_skills.append({'name': name, 'level': int(level)})

    def is_running(self, current_day):
        return self.completition_day and self.completition_day <= current_day

    def is_available(self):
        return self.completition_day is None

    def start(self, current_day):
        self.completition_day = current_day + self.days


class Output:
    def __init__(self, filename):
        self.filename = filename
        self.lines = []


    def create_line(self,project_name, collaborators):
        self.lines.append('{}\n{}\n'.format(project_name," ".join(collaborators)))

    def write(self):
        n_projects = len(self.lines)
        f = open(self.filename, "w")
        f.write(str(n_projects)+'\n')
        for l in self.lines:
            f.write(l)
        f.close()




def find_best_collaborator(current_day, skill, project, collaborators):
    skill_elegible = [c for c in collaborators if
                      skill['name'] in c.skills and c.skills[skill['name']] >= skill['level'] and c.is_free(
                          current_day)]
    skill_elegible.sort(key=lambda c: c.skill_level)
    if skill_elegible:
        collaborators.remove(skill_elegible[0])
        return skill_elegible[0]


def readFile(filename):
    collaborators = {}
    projects = {}
    with open(filename) as file:
        contributors, read_projects = map(int, file.readline().split(' '))
        for c in range(contributors):
            name, skills = file.readline().split(' ')
            collaborator = Collaborator(name)
            for s in range(int(skills)):
                skill, level = file.readline().split(' ')
                level = int(level)
                collaborator.addSkill(skill, level)

            collaborators[collaborator.name] = collaborator
        for p in range(read_projects):
            name, days, score, best_before, number_of_roles = file.readline().split(' ')
            project = Project(name, days, score, best_before, number_of_roles)
            for ps in  range(int(number_of_roles)):
                skill, level = file.readline().split(' ')
                project.add_required_skills(skill, level)
            projects[project.name] = project

    print('collaborators', len(collaborators), 'projects', len(projects))
    final_deadline = max([p.best_before for p in projects.values()])
    print('best before', min([p.best_before for p in projects.values()]), )



    output = Output(f'{filename}.out')
    for current_day in range(final_deadline):
        values = get_possible_projects(current_day, projects, collaborators)
        values = sorted(values, key=lambda r: r[0].score, reverse=True)
        for project, eligible_collaborators in values:
            assigned_collaborators = []
            for skill in project.required_skills:
                best_for_skill = find_best_collaborator(current_day, skill, project, eligible_collaborators)
                if best_for_skill:
                    assigned_collaborators.append(best_for_skill)

            if len(assigned_collaborators) == project.number_of_roles:
                project.start(current_day)
                output.create_line(project.name, [c.name for c in assigned_collaborators])
                print(current_day, project.score, assigned_collaborators)
                for c in assigned_collaborators:
                    c.assign(current_day, project, skill)
                    # if c.skills[skill['name']] == skill['level']:
                    #     c.level_up_skill(skill['name'])
                    c.release_day = current_day - 1 + project.days

                pass

    output.write()
    return projects, collaborators

def find_possible_collaborators(project, collaborators):
    filtered = []
    for c in collaborators.values():
        for skill in project.required_skills:
            if skill['name'] in c.skills and c.skills[skill['name']] >= skill['level']:
                filtered.append(c)
                break
    return filtered


def get_possible_projects(current_day, projects, collaborators):
    values = []
    for p in projects.values():
        if p.is_available():
            possible_collaborators = find_possible_collaborators(p, collaborators)
            if possible_collaborators:
                values.append([p, possible_collaborators])
    return values

#  collaborators = [c.name for c in collaborators]
# readFile('./input_data/a_an_example.in.txt')
# readFile('./input_data/d_dense_schedule.in.txt')
readFile('./input_data/b_better_start_small.in.txt')
# readFile('./input_data/e_exceptional_skills.in.txt')
# readFile('./input_data/c_collaboration.in.txt')
# readFile('./input_data/f_find_great_mentors.in.txt')
